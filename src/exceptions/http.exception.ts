export class HttpException extends Error {
    constructor(public code: number, error: Error | string = '') {
      super(error instanceof Error ? error.message : error);
      this.message = `${getMessageByCode(code)} ${this.message}`;
    }
  }
  
  export const getMessageByCode = (code: number) => {
    switch (code) {
      case 400: return 'Solicitação inválida.';
      case 401: return 'Acesso autorizado apenas para usuários autenticados.';
      case 402: return 'Ops! Parece que você não possui um plano ativo. Você precisa de um plano ativo para utilizar essa funcionalidade, para isto basta clicar sobre a imagem do seu perfil e escolher um plano!';
      case 403: return 'Você não tem permissão para acessar essa informação.';
      case 404: return 'Não encontrado.';
      case 405: return 'Método não permitido.';
      case 500: return 'Erro interno no servidor.';
      case 501: return 'Esta função ainda não foi implementada.';
      case 503: return 'Serviço indisponível.';
      default: return 'Erro interno no servidor.';
    }
  };
  
  export const httpErrors = (code, message?) => {
    return {
      code, 
      message: `${getMessageByCode(code)} ${message}`.trim() 
    }
  };
  
  export default HttpException;
  