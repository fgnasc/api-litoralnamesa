import conn from "../db";
import phpUnserialize from "phpunserialize";
import moment from "moment";
import HttpException from "../exceptions/http.exception";
export class ProductsService {
  static async getProducts(companyId): Promise<any> {
    try {
      let [response]: any = await conn.execute(
        `CALL get_products (${companyId})`
      );
      console.log(response);
      response = response[0].reduce((ac, x) => {
        ac[x.IDProduct] = ac[x.IDProduct] || [];

        ac[x.IDProduct].push(x);
        return ac;
      }, Object.create(null));
      return response;
    } catch (error) {
      throw error;
    }
  }
  static async getGroupOptions(companyId): Promise<any> {
    try {
      const [response]: any = await conn.execute(
        `CALL get_group_option (${companyId})`
      );
      console.log(response);

      return response[0];
    } catch (error) {
      throw error;
    }
  }
  static async getSizeInfo(companyId): Promise<any> {
    try {
      let [response]: any = await conn.execute(
        `CALL  get_size_info (${companyId})`
      );
      response = response[0].reduce((ac, x, i, arr) => {
        ac[x.IDVariety] = ac[x.IDVariety] || [];
        ac[x.IDVariety].push(x);

        return ac;
      }, Object.create(null));

      return response;
    } catch (error) {
      throw error;
    }
  }
  static async changeStatus(body, companyId): Promise<any> {
    try {
      const [response]: any = await conn.execute(`CALL get_products ()`);
      return response[0];
    } catch (error) {
      throw error;
    }
  }
  static async updateVariety(body, companyId): Promise<any> {
    try {
      const groupOptions = body.GroupOptions;

      if (!/^(?=.*\d)\d*(?:\.\d\d)?$/.test(body.VarietyValue))
        throw Error("Preço da variedade inválido");

      groupOptions.forEach((group) => {
        group.Options.forEach((option) => {
          if (!/^(?=.*\d)\d*(?:\.\d\d)?$/.test(option.OptionValue))
            throw Error("Preço da opção inválido");
        });
      });
      Promise.all([
        ...groupOptions.map(async (group) =>
          conn.execute(
            `CALL update_group_options( ${group.IDGroupOption} ,  '${
              group.GroupName
            }' , ${group.GroupOptionMax} , ${group.GroupOptionMin} , '${
              group.GroupOptionRequired ? 1 : -1
            }','${group.GroupOptionStatus ? 1 : -1}')`
          )
        ),
        ...groupOptions.map((group) => {
          group.Options.map((option) =>
            conn.execute(
              `CALL update_option( ${option.IDOption} ,  '${
                option.OptionName
              }' , ${option.OptionValue} , ${option.VarietyOptionMax} , ${
                option.VarietyOptionMin
              }, '${option.OptionStatus ? 1 : -1}', '${
                option.VarietyOptionsStatus ? 1 : -1
              }', ${body.IDVariety})`
            )
          );
        }),
        conn.execute(
          `CALL update_variety( ${body.IDVariety} ,  '${body.VarietyName}' , '${
            body.VarietyDescription
          }' , '${body.VarietyValue}' , ${body.MinOptions} , ${
            body.MaxOptions
          } , ${body.VarietyStatus ? 1 : -1})`
        ),
      ]);
      return body;
    } catch (error) {
      throw error;
    }
  }
  static async getProductPopulated(companyId): Promise<any> {
    try {
      let [sizeInfo]: any = await conn.execute(
        `CALL  get_size_info (${companyId})`
      );
      sizeInfo = sizeInfo[0].reduce((ac, x, i, arr) => {
        ac[x.IDVariety] = ac[x.IDVariety] || [];
        ac[x.IDVariety].push(x);

        return ac;
      }, Object.create(null));

      let [response]: any = await conn.execute(
        `CALL get_products (${companyId})`
      );
      response = response[0].reduce((ac, x) => {
        ac[x.IDProduct] = ac[x.IDProduct] || [];
        x.GroupOptions = sizeInfo[x.IDVariety] ? sizeInfo[x.IDVariety] : null;
        ac[x.IDProduct].push(x);
        return ac;
      }, Object.create(null));
      response = Object.keys(response)
        .map((k) => ({
          OrderList: response[k][0].order_list,
          ProductName: response[k][0].ProductName,
          Varieties: response[k],
        }))
        .sort((x: any, y: any) => {
          if (x.OrderList > y.OrderList) return 1;
          if (x.OrderList < y.OrderList) return -1;
        });
      response.forEach((y) => {
        y.Varieties.forEach((x) => {
          x.GroupOptions
            ? (x.GroupOptions = x.GroupOptions.reduce((ac, w) => {
                const found = ac.find(
                  (z) => z.IDGroupOption == w.IDGroupOption
                );
                if (found) {
                  found.Options.push(w);
                } else {
                  ac.push({
                    Options: [w],
                    IDGroupOption: w.IDGroupOption,
                    GroupName: w.GroupOptionName,
                    GroupOptionMax: w.GroupOptionMax,
                    GroupOptionMin: w.GroupOptionMin,
                    GroupOptionRequired: w.GroupOptionRequired,
                  });
                }
                return ac;
              }, []))
            : null;
        });
      });
      return response;
    } catch (error) {
      throw error;
    }
  }
}
