import WebSocketWrapper from "../server/socket-channels";
import { OrdersService } from "./orders.service";

const SEC = 1000;
const MIN = SEC * 60;

export class OrdersTimeoutService {
	static openedOrdersList: OpenedOrder[] = [];

	static addOpenedOrder(IDOrder: number, IDCompany: number) {
		this.openedOrdersList.push(new OpenedOrder(IDOrder, IDCompany));
	}

	static getOpenedOrderById(IDOrder: number) {
		return this.openedOrdersList.find((x) => x.IDOrder === IDOrder);
	}

	static removeOpenedOrder(openedOrder: OpenedOrder | number) {
		const newOpenedOrder: OpenedOrder = typeof openedOrder === "number" ? this.getOpenedOrderById(+openedOrder) : openedOrder;
    const index = this.openedOrdersList.findIndex((x) => x.IDOrder === newOpenedOrder.IDOrder);
		if (index !== -1) {
			this.openedOrdersList.splice(index, 1);
		}
		clearInterval(newOpenedOrder.interval);
	}
}

export class OpenedOrder {
	interval: NodeJS.Timeout;
	timeLeft: number = 10 * SEC;

	get toTransport() {
		return {
			IDOrder: this.IDOrder,
			timeLeft_ms: this.timeLeft,
			timeLeft: this.timeLeft > MIN ? `${this.timeLeft / MIN} Minutos` : `${this.timeLeft / 1000} Segundos`,
		};
	}

	constructor(public readonly IDOrder: number, public readonly IDCompany: number, public createdAt: Date = new Date()) {
		this.controlOrderEvent();
		this.interval = setInterval(this.subTimeAndControlEvents.bind(this), SEC);
	}

	subTimeAndControlEvents() {
		this.timeLeft -= this.timeLeft > MIN ? MIN : SEC;
		this.controlOrderEvent();
	}

	controlOrderEvent() {
		WebSocketWrapper.ordersNS.to(this.IDCompany).emit("opened-order-time-left", this.toTransport);
		if (this.timeLeft === MIN) {
			clearInterval(this.interval);
			this.interval = setInterval(this.subTimeAndControlEvents.bind(this), SEC);
		} else if (this.timeLeft <= 0) {
			try {
				OrdersService.cancelAndNotify(this.IDOrder, this.IDCompany, "O restaurante não respondeu a tempo.");
			} catch (error) {
				console.error("Falha ao cancelar pedido!");
			} finally {        
				OrdersTimeoutService.removeOpenedOrder(this);
			}
		}
	}
}
