import HttpException, { httpErrors } from "../exceptions/http.exception";
import jwt from "jsonwebtoken";
import conn from "../db";
import { SHA1 } from "crypto-js";

export class AuthenticationService {
	static async login(email: string, password: string, remember: boolean = false): Promise<any> {
		try {
			const user = (
				await conn.query(`
			SELECT sysu.IDCompany, sysu.Email, sysu.IDSystemUser, sysu.Password, sysu.DisplayName, sysu.Type, 
			company.Name, 
			company.CompanyName, 
			company.Telefone, 
			company.TelCobranca, 
			company.Celular, 
			company.Promotion, 
			company.Description,
			company.OrderMinimumValue,
			company.ShowLogo,
			company.Destaque,
			company.IDMembershipPlan,
			company.SendSms,
			company.Status,
			company.StatusCron,
			company.Moderate,
			company.ChargeProduct,
      		deliverytype.Type, 
      		deliverytype.DeliveryTime
			FROM sysu 
      		LEFT JOIN company ON sysu.IDCompany = company.IDCompany
      		INNER JOIN deliverytype ON company.IDCompany = deliverytype.IDCompany
			WHERE sysu.Email = '${email}' AND deliverytype.Type = 'delivery'`)
			)[0][0];
			if (!user) throw Error("Usuário não encontrado");
			if (user.Password != SHA1(password)) throw Error("Senhas não conferem");
			const exp = remember ? 604800 : 86400;
			const token = jwt.sign({ id: user.IDSystemUser, shopkeeper: user.IDCompany }, process.env.JWT_KEY, {
				expiresIn: exp,
			});

			delete user.ApiKey;
			delete user.Password;
			return { auth: true, token, user };
		} catch (error) {
			console.log(error);
			throw error;
		}
	}
}

export default new AuthenticationService();
