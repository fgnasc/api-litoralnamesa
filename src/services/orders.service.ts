import conn from "../db";
import phpUnserialize from "phpunserialize";
import HttpException from "../exceptions/http.exception";
import WebSocketWrapper from "../server/socket-channels";
import { OpenedOrder, OrdersTimeoutService } from "./orders-timeout.service";
export class OrdersService {
	static async getOrders(companyId, query): Promise<any> {
		try {
			["date", "query"].forEach((k) => (query[k] = query[k] || ""));
			if (!query.status) query.status = "Open,Accepted,Dispatched,Delivered,Canceled";

			const [response] = await conn.execute(`CALL get_filtered_orders('${query.query}', '${query.date}', '${query.status}', ${companyId})`);

			response[0].sort((x: any, y: any) => {
				if (x.Status == "Open" && y.Status != "Open") return -1;
				if (x.Status != "Open" && y.Status == "Open") return 1;

				if (x.Status == "Accepted" && y.Status != "Accepted") return -1;
				if (x.Status != "Accepted" && y.Status == "Accepted") return 1;

				if (x.DateTime > y.DateTime) return -1;
				if (x.DateTime < y.DateTime) return 1;
			});

			return response[0];
		} catch (error) {
			console.log(error);
			throw error;
		}
	}
	static async getOrderById(orderId, companyId): Promise<any> {
		try {
			let [response]: any = await conn.execute(`CALL get_order(${orderId}, ${companyId})`);
			response = response[0][0];

			response.Status != "Open" ? (response.CustomerAddress = phpUnserialize(response.CustomerAddress)[0]) : "";
			response.Products = phpUnserialize(response.Products);
			response.Values = phpUnserialize(response.Values);
			response.Varieties = phpUnserialize(response.Varieties);
			response.Sizes = phpUnserialize(response.Sizes);
			response.Quantity = phpUnserialize(response.Quantity);
			response.Options = phpUnserialize(response.Options);
			response.Notes = phpUnserialize(response.Notes);

			const productAndVariety = response.Products.map((product, i) => {
				return {
					Product: response.Products[i],
					Varieties: response.Varieties[i],
					Values: response.Values[i],
					Sizes: response.Sizes[i],
					Quantity: response.Quantity[i],
					Options: response.Options[i],
					Notes: response.Notes[i],
					priceOptions: response.Options[i] ? response.Options[i].reduce((ac, x) => ac + parseFloat(x.Value), 0) : 0,
				};
			});

			let productVarietyPopulated = (await conn.execute(`CALL product_variety('${response.Products.join()}', '${response.Varieties.join()}');`))[0][0];

			productAndVariety.forEach((product) => {
				product.Product = productVarietyPopulated.find((x) => x.IDProduct == product.Product).ProductName;
				product.Varieties = product.Varieties.map((variety) => productVarietyPopulated.find((x) => x.IDVariety == variety));
				if (
					product.Product.toUpperCase().includes("PIZZA") &&
					(product.Product.toUpperCase().includes("MEIA") || product.Product.toUpperCase().includes("PROMOÇÃO") || product.Varieties.length >= 2)
				) {
					product.Varieties.forEach((variety) => {
						variety.VarietyName = `MEIA - ${variety.VarietyName}`;
					});
					product.Values = product.Varieties.reduce((ac, x) => {
						return +ac.VarietyPrice > +x.VarietyPrice ? +ac.VarietyPrice : +x.VarietyPrice;
					}, 0);
					product.Varieties.forEach((x) => {
						if (x.VarietyPrice != product.Values) {
							x.VarietyPrice = null;
						}
					});
				}
				product.Values = (parseFloat(product.Values) + product.priceOptions) * product.Quantity;
			});

			delete response.Quantity;
			delete response.Values;
			delete response.Sizes;
			delete response.Options;
			delete response.Notes;
			delete response.Varieties;

			if (response.DeliveryTime[1] == ":") {
				response.DeliveryTime = "0" + response.DeliveryTime;
			}
			response.DeliveryTime = response.DeliveryTime.substr(0, 5);
			response.Products = productAndVariety;
			response.TotalProducts =
				response.Status != "Open"
					? (+response.ChangeForMoney - response.ChangeFor - response.ValueDelivery).toFixed(2)
					: productAndVariety.reduce((ac, x) => ac + x.Values, 0);
			return response;
		} catch (error) {
			console.log(error);

			if (!error.ProductName) {
				throw Error("O produto deste pedido não existe mais!");
			}
			console.log(error);
			throw error;
		}
	}

	static async updateStatus(orderId: number, companyId, body) {
		try {
			let { status, deliveryTime, cancelMessage, Courier } = body;
			const validStatus = ["Open", "Waiting", "Accepted", "Paid", "Dispatched", "Delivered", "Canceled"];
			let response: any;

			if (!status || validStatus.findIndex((x) => x == status) < 0) {
				throw Error("Valor do status inválido");
			}
			if (deliveryTime) deliveryTime = new Date(new Date().setMinutes(new Date().getMinutes() + +deliveryTime)).toLocaleTimeString();

			switch (status) {
				case "Accepted":
					[response] = await conn.execute(`CALL update_status(
						'${status}',
						${orderId},
						${companyId},
						'${deliveryTime}',
						'')`);
					OrdersTimeoutService.removeOpenedOrder(+orderId);
					break;
				case "Canceled":
					[response] = await conn.execute(`CALL cancel_order( 
						${orderId},
						${companyId},
						'${cancelMessage}')`);
					break;
				case "Dispatched":
					[response] = await conn.execute(`CALL update_status(
						'${status}',
						${orderId},
						${companyId},
						null,
						'${Courier}')`);
					break;
			}
			return response[0];
		} catch (error) {
			console.log(error);
			throw error;
		}
	}

	static async cancelAndNotify(IDOrder: number, IDCompany: number, cancelMessage: string) {
		console.log("canceled");
		const [response] = await conn.execute(`CALL cancel_order( 
							${IDOrder},
							${IDCompany},
							'${cancelMessage}')`);

		WebSocketWrapper.ordersNS.to(IDCompany).emit("cancel-order", {
			IDOrder,
			IDCompany,
			cancelMessage,
		});
		return response[0];
	}
	static async getOrdersCount(customerId, companyId) {
		const [response] = await conn.execute(`CALL  get_user_quantity_orders( 
						${customerId},
						${companyId})`);
		return response[0];
	}
}
