import conn from "../db";
import phpUnserialize from "phpunserialize";
import moment from "moment";
import HttpException from "../exceptions/http.exception";
export class CompanyService {
	static async getOpeningHour(companyId): Promise<any> {
		try {
			const openingHours: any = (await conn.query(`SELECT IDOpening, Name, Day, Open, Close FROM default_opening WHERE IDCompany = ${companyId} `))[0];

			openingHours.forEach((open, i) => {
				open.Day = open.Day.split(",").map((x) => +x);
				open.Open = moment(open.Open, "HH:mm").format("HH:mm");
				open.Close = moment(open.Close, "HH:mm").format("HH:mm");
			});

			return openingHours;
		} catch (error) {
			throw error;
		}
	}
	static async getSchedules(companyId): Promise<any> {
		try {
			let schedule: any = (await conn.query(`SELECT IDSchedule, Name, schDate, Open, Close FROM schedule_refactor WHERE IDCompany = ${companyId} `))[0];

			schedule.forEach((sch, i) => {
				sch.Open = moment(sch.Open, "HH:mm").format("HH:mm");
				sch.Close = moment(sch.Close, "HH:mm").format("HH:mm");
			});

			schedule = schedule.reduce((ac, sch, i, arr) => {
				sch.schDate = moment(sch.schDate).format("DD/MM/yyyy");
				const found = arr.filter((x) => x.IDSchedule == sch.IDSchedule);
				if (found.length > 1) {
					found[0].Open > found[1].Open ? found.splice(1, 1) : found.splice(0, 1);
					if (ac.some((x) => x.IDSchedule == found[0].IDSchedule)) return ac;

					ac.push(found[0]);
				} else {
					ac.push(sch);
				}

				return ac;
			}, []);
			return schedule;
		} catch (error) {
			throw error;
		}
	}
	static async insertHours(companyId, body): Promise<any> {
		try {
			body.forEach((x, i) => {
				body.forEach((y, j) => {
					if (i == j) return;
					if (x.Day.some((x) => y.Day.includes(x))) {
						if (interceptHours(x.Open, x.Close, y.Open, y.Close)) throw Error("Você não pode inserir horários que se interceptam.");
					}
				});
			});
			await Promise.all([
				body.map(
					async (x) =>
						await conn.execute(`CALL alter_opening (${x.IDOpening},'${x.Name}', '${x.Day.join(",")}', '${x.Open}', '${x.Close}', ${companyId}, ${x.IDMenu})`)
				),
			]);

			return;
		} catch (error) {
			throw error;
		}
	}
	static async insertSchedules(companyId, schedule): Promise<any> {
		try {
			let schedules = [schedule];
			if (schedule.Open > schedule.Close) {
				const date = new Date(schedule.schDate);
				date.setDate(date.getDate() + 1);
				schedules.push({
					IDSchedule: schedule.IDSchedule,
					Name: schedule.Name,
					schDate: date,
					Open: "00:00",
					IDMenu: schedule.IDMenu,
					Close: schedule.Close,
				});
			}
			schedules.forEach(async (x, i) => {
				x.schDate = new Date(x.schDate);
				x.schDate = x.schDate.toISOString().split("T")[0];
				// CALL insert_opening(null, 'teste 6', '1', '10:00:00', '14:00:00', 124, 0)
				await conn.execute(`CALL insert_schedule (${x.IDSchedule}, '${x.schDate}', '${x.Open}', '${x.Close}', '${x.Name}' , ${companyId}, ${x.IDMenu})`);
			});

			return;
		} catch (error) {
			throw error;
		}
	}
	static async setAutomaticSchedule(status, companyId): Promise<any> {
		try {
			await conn.execute(`CALL set_automatic_schedule (${status}, ${companyId})`);

			return;
		} catch (error) {
			throw error;
		}
	}
	static async setCompanyStatus(status, companyId): Promise<any> {
		try {
			await conn.execute(`CALL set_company_status (${status}, ${companyId})`);

			return;
		} catch (error) {
			throw error;
		}
	}
	static async deleteSchedules(companyId, id): Promise<any> {
		try {
			await conn.execute(`CALL delete_schedule (${id}, ${companyId})`);
			return;
		} catch (error) {
			throw error;
		}
	}
}
function interceptHours(firstOpen, firstClose, secondOpen, secondClose) {
	if (
		(secondOpen > firstOpen && secondOpen < firstClose) ||
		(secondClose > firstOpen && secondClose < firstClose) ||
		(secondOpen < firstOpen && firstClose < secondClose) ||
		(firstOpen < secondOpen && secondClose < firstClose)
	) {
		return true;
	}
	return false;
}
