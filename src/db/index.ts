import mysql from "mysql2/promise";

const conn = mysql.createPool({
	host: process.env.HOST,
	user: 'root',
	database: process.env.DATABASE,
	waitForConnections: true,
	connectionLimit: 10,
	queueLimit: 0,
	// port: ''
});

export default conn;
