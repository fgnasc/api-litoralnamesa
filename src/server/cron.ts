import cron from "node-cron";
import conn from "../db";

const cronExample = cron.schedule(
	"*/5 * * * * *",
	async () => {
		const today = new Date();
		const time = today.toLocaleTimeString().slice(0, 5);
		// today.setHours(today.getHours() + 1);
		const dayNumber = today.getDay() == 0 ? 7 : today.getDay();
		// today.setHours(11);
		// today.setMinutes(25);
		// today.setDate(22);

		const found: any[][] = await conn.execute(`CALL  get_open_companies(${dayNumber}, ${today.getDate()}, ${today.getMonth() + 1}, ${today.getFullYear()})`);
		let [companiesDefault, companiesSchedule] = found[0].slice(0, 2);

		let companiesClosed = [];
		let companiesOpen = [];

		companiesSchedule.forEach((sch) => {
			var found = companiesDefault.filter((x) => x.IDCompany == sch.IDCompany);
			found.forEach((def, i) => {
				if (interceptHours(sch.Open, sch.Close, def.Open, def.Close)) {
					companiesDefault.splice(i, 1);
				}
			});
		});
		companiesDefault = parseResult(companiesDefault, today)
		companiesSchedule = parseResult(companiesSchedule, today)
		Object.keys(companiesSchedule).forEach((key) => {
			companiesSchedule[key].forEach((sch) => {
				if (key == "open" && companiesOpen.findIndex((x) => x == sch.IDCompany) < 0) {
					companiesOpen.push(sch.IDCompany);
				} else if (key == "close" && companiesClosed.findIndex((x) => x == sch.IDCompany) < 0) {
					companiesClosed.push(sch.IDCompany);
				}
			});
		});

		Object.keys(companiesDefault).forEach((key) => {
			companiesDefault[key].forEach((sch) => {
				if (companiesOpen.findIndex((p) => p == sch.IDCompany) < 0 && companiesClosed.findIndex((p) => p == sch.IDCompany) < 0) {
					key == "open" ? companiesOpen.push(sch.IDCompany) : companiesClosed.push(sch.IDCompany);
				}
			});
		});
		// await conn.execute(`CALL change_companies_status('${companiesOpen.join(",")}', '${companiesClosed.join(",")}')`);

		console.log(cronExample.getStatus());
	},
	{
		scheduled: false,
		timezone: "America/Sao_Paulo",
	}
);

function interceptHours(schOpen, schClose, defOpen, defClose) {
	if (
		(defOpen > schOpen && defOpen < schClose) ||
		(defClose > schOpen && defClose < schClose) ||
		(defOpen < schOpen && schClose < defClose) ||
		(schOpen < defOpen && defClose < schClose)
	) {
		return true;
	}
	return false;
}

function parseResult(result: any[], today: Date) {
	const time = today.toLocaleTimeString().slice(0, 5);
	return result.reduce(
		(acc, x) => {
			if (x.Open.includes(time)) {
				acc.open.push(x);
			} else if (x.Close.includes(time)) {
				acc.close.push(x);
			}
			return acc;
		},
		{ open: [], close: [] }
	);
}

export default cronExample;
