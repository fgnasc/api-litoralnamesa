import { Request, Response, NextFunction } from "express";
import moment from "moment";
import conn from "../../db";

export const opening = () => {
	return async (req: any, res: Response, next: NextFunction) => {
		const today = new Date();
		const allowed = req.openingHours.map((open, i) => {
			const days = open.Day.split(",");
			if (
				days.some((day) => day == (today.getDay() == 0 ? 7 : today.getDay())) &&
				today.toLocaleTimeString() >= moment(open.Opens, "HH:mm:ss").format("HH:mm:ss") &&
				today.toLocaleTimeString() <= moment(open.Closes, "HH:mm:ss").format("HH:mm:ss")
			) {
				return true;
			} else {
				return false;
			}
		});
		if (!allowed.some((x) => x)) {
			res.status(400).json("Seu restaurante se encontra fechado!");
			next();
		}
		next();
	};
};
