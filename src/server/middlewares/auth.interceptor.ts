import { Request, Response, NextFunction } from "express";
import { verify } from "jsonwebtoken";
import conn from "../../db";
import { httpErrors } from "../../exceptions/http.exception";

const HTTP_NOT_AUTHORIZED = { auth: false, error: httpErrors(401) },
	HTTP_FORBIDDEN = { auth: false, error: httpErrors(403) };

export const authenticateToken = async (token: string): Promise<any | null> => {
	try {
		let id: string;
		try {
			const data = verify(token, process.env.JWT_KEY);
			if (data) id = data.id;
		} catch (err) {
			console.error(err);
			return null;
		}
		const company = (await conn.query(`SELECT Type, IDCompany as ID FROM sysu where IDSystemUser = ${id}`))[0][0];

		return company || null;
	} catch (error) {
		console.error(error);
		return null;
	}
};

export const authorize = (...allowed: string[]) => {
	const isAllowed = (Type) => allowed.indexOf(Type) > -1 || allowed.indexOf("all") > -1;

	return async (req: any, res: Response, next: NextFunction) => {
		if (req.header("Authorization")) {
			const token = req.header("Authorization").replace("Bearer ", "");

			if (!token) {
				console.log(`Token not found`);
				res.status(401).json(HTTP_NOT_AUTHORIZED);
				next();
				return;
			}
			const company = await authenticateToken(token);
			if (!company) {
				console.log(`Company not found`);
				res.status(401).json(HTTP_NOT_AUTHORIZED);
				next();
				return;
			}
			if (!(company && isAllowed(company.Type))) {
				console.log(`Company not allowed to access this route`);
				res.status(403).json(HTTP_FORBIDDEN);
			}

			req.company = company;

			next();
		} else {
			res.status(401).json(HTTP_NOT_AUTHORIZED);
			next();
		}
	};
};
