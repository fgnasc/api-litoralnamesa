import http from "http";
import { authenticateToken } from "../middlewares/auth.interceptor";
const socketIO = require("socket.io");

export default class WebSocketWrapper {
  static socketServer: any;

  static ordersNS;
  static chatNS;

  static initSocketServer(server: http.Server) {
    this.socketServer = socketIO(server, { cors: "*" });

    this.ordersNS = this.socketServer.of('/orders');
    this.ordersNS.use(this.authSocket);
    this.ordersNS.on("connection", this.joinCompany);

    this.chatNS = this.socketServer.of('/chat')
    this.chatNS.use(this.authSocket);
    this.chatNS.on("connection", this.joinCompany);

  }

  private static authSocket = async (socket, next) => {
    const user = await authenticateToken(socket.handshake.auth.token);
    if (user) {
      socket.handshake.auth.IDCompany = user.ID;
      console.log("Socket autenticado!");
      next();
    } else {
      console.log("Socket não autenticado!");
      next(new Error("invalid"));
    }
  }

  private static joinCompany = (socket) => {
    socket.join(socket.handshake.auth.IDCompany);
    console.log(`Nova conexão socket para empresa ${socket.handshake.auth.IDCompany}!`);
  }
}
