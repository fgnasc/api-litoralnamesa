import express from "express";

import { CompanyController } from "../../controllers/company.controller";
import { authorize } from "../middlewares/auth.interceptor";
import { opening } from "../middlewares/open.interceptor";

const Controller = new CompanyController();

export default express.Router()
.get("/opening-hours", authorize("admin"), Controller.getOpeningHour)
.get("/schedules", authorize("admin"), Controller.getSchedules)
.post("/default", authorize("admin"), Controller.insertHours)
.post("/schedule", authorize("admin"), Controller.insertSchedules)
.patch("/automatic-schedule/:status", authorize("admin"), Controller.setAutomaticSchedule)
.patch("/company-status/:status", authorize("admin"), Controller.setCompanyStatus)
.delete("/schedule/:id", authorize("admin"), Controller.deleteSchedules)
