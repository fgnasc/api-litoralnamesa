import express from "express";
import { Application } from "express";
import authRoutes from "./authentication.routes";
import ordersRoutes from "./orders.routes";
import cronRoutes from "./cron.routes";
import companyRoutes from "./company.routes";
import productsRoutes from "./products.routes";
import chatRoutes from "./chat.route";
const apiBasePath = "/api/v1";

export default function routes(app: Application): void {
	app.use(`${apiBasePath}/auth`, authRoutes);
	app.use(`${apiBasePath}/orders`, ordersRoutes);
	app.use(`${apiBasePath}/company`, companyRoutes);
	app.use(`${apiBasePath}/products`, productsRoutes);
	app.use(`${apiBasePath}/cron`, cronRoutes);
	app.use(`${apiBasePath}/chat`, chatRoutes);
	app.use(express.static("public"));
}
