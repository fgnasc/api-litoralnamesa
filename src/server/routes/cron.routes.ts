import express from "express";
import cronExample from "../cron";

export default express
	.Router()
	.get("/get-cron-status", (req, res) => {
		res.status(200).send(cronExample.getStatus());
	})
	.get("/start-cron", (req, res) => {
		cronExample.start(),
		res.status(200).send("Cron iniciada");
	})
	.get("/stop-cron", (req, res) => {
		cronExample.stop();
		res.status(200).send("Cron parada");
	});
