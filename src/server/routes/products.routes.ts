import express from "express";

import { ProductsController } from "../../controllers/products.controller";
import { authorize } from "../middlewares/auth.interceptor";

const Controller = new ProductsController();

export default express
	.Router()
	.get("/products", authorize("admin"), Controller.getProducts)
	.get("/products-populated", authorize("admin"), Controller.getProductPopulated)
	.get("/group-options", authorize("admin"), Controller.getGroupOptions)	
	.get("/size-info", authorize("admin"), Controller.getSizeInfo)	
	.patch("/status", authorize("admin"), Controller.changeStatus)
	.put("/update-variety", authorize("admin"), Controller.updateVariety)