import express from "express";
import { ChatController } from "../../controllers/chat.controller";
import { authorize } from "../middlewares/auth.interceptor";

const Controller = new ChatController();

export default express
  .Router()
  .get("/chat", authorize("all"), Controller.listChats)
  .get("/chat/:id", authorize("all"), Controller.getChat)
  .post("/chat", authorize("all"), Controller.sendMessage);