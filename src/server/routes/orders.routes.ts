import express from "express";

import { OrdersController } from "../../controllers/orders.controller";
import { authorize } from "../middlewares/auth.interceptor";
import { opening } from "../middlewares/open.interceptor";

const Controller = new OrdersController();

export default express
	.Router()
	.get("/", authorize("admin"), Controller.getOrders)
	.get("/:id", authorize("admin"), Controller.getOrderById)
	.get("/get-quantity-orders/:id", authorize("admin"), Controller.getOrdersCount)
	.post("/:id/update-status", authorize("admin"), Controller.updateStatus)
	.post("/new-order-example", Controller.socketTest)
