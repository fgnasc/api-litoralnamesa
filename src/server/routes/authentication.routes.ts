import express from "express";

import { AuthenticationController } from "../../controllers/authentication.controller";
import { authorize } from "../middlewares/auth.interceptor";

const Controller = new AuthenticationController();

export default express.Router().post("/login", Controller.login);
