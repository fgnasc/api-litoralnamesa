import { NextFunction, Request, Response } from "express";
import { HttpException } from "../exceptions/http.exception";
import { CompanyService } from "../services/company.service";

export class CompanyController {
	async getOpeningHour(req: any, res: Response, next: NextFunction): Promise<void> {
		try {
			const response = await CompanyService.getOpeningHour(req.company.ID);
			res.status(200).json(response);
		} catch (error) {
			console.log(error);
			next(new HttpException(400, error));
		}
	}
	async getSchedules(req: any, res: Response, next: NextFunction): Promise<void> {
		try {
			const response = await CompanyService.getSchedules(req.company.ID);
			res.status(200).json(response);
		} catch (error) {
			console.log(error);
			next(new HttpException(400, error));
		}
	}
	async insertHours(req: any, res: Response, next: NextFunction): Promise<void> {
		try {
			await CompanyService.insertHours(req.company.ID, req.body);
			res.status(200).json("OK");
		} catch (error) {
			console.log(error);
			next(new HttpException(400, error));
		}
	}
	async insertSchedules(req: any, res: Response, next: NextFunction): Promise<void> {
		try {
			await CompanyService.insertSchedules(req.company.ID, req.body);
			res.status(200).json("OK");
		} catch (error) {
			console.log(error);
			next(new HttpException(400, error));
		}
	}
	async deleteSchedules(req: any, res: Response, next: NextFunction): Promise<void> {
		try {
			await CompanyService.deleteSchedules(req.company.ID, req.params.id);
			res.status(200).json("OK");
		} catch (error) {
			console.log(error);
			next(new HttpException(400, error));
		}
	}
	async setAutomaticSchedule(req: any, res: Response, next: NextFunction): Promise<void> {
		try {
			await CompanyService.setAutomaticSchedule(req.company.ID, req.params.status);
			res.status(200).json("OK");
		} catch (error) {
			console.log(error);
			next(new HttpException(400, error));
		}
	}
	async setCompanyStatus(req: any, res: Response, next: NextFunction): Promise<void> {
		try {
			await CompanyService.setCompanyStatus(req.company.ID, req.params.status);
			res.status(200).json("OK");
		} catch (error) {
			console.log(error);
			next(new HttpException(400, error));
		}
	}
}
