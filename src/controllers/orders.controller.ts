import { NextFunction, Request, Response } from "express";
import { HttpException } from "../exceptions/http.exception";
import { OrdersService } from "../services/orders.service";
import WebSocketWrapper from "../server/socket-channels";
import { OrdersTimeoutService } from "../services/orders-timeout.service";

export class OrdersController {
	async getOrders(req: any, res: Response, next: NextFunction): Promise<void> {
		try {
			// if (!req.query.page || !req.query.many || !req.query.company) {
			// 	throw Error("Parâmetros informados incorretamente");
			// }
			const response = await OrdersService.getOrders(req.company.ID, req.query);
			res.status(200).json(response);
		} catch (error) {
			console.log(error);
			next(new HttpException(400, error));
		}
	}

	async getOrderById(req: any, res: Response, next: NextFunction): Promise<void> {
		try {
			if (!req.params.id) {
				throw Error("ID inexistente ou inválido");
			}
			const response = await OrdersService.getOrderById(req.params.id, req.company.ID);
			res.status(200).json(response);
		} catch (error) {
			console.log(error);
			next(new HttpException(400, error));
		}
	}
	async getOrdersCount(req: any, res: Response, next: NextFunction): Promise<void> {
		try {
			if (!req.params.id) {
				throw Error("ID inexistente ou inválido");
			}
			const response = await OrdersService.getOrdersCount(req.params.id, req.company.ID);

			res.status(200).json(response[0].ordersQTD);
		} catch (error) {
			console.log(error);
			next(new HttpException(400, error));
		}
	}
	async socketTest(req: any, res: Response, next: NextFunction): Promise<void> {
		try {
			if (!req.body?.IDCompany) {
				throw Error("ID da empresa inexistente ou inválido");
			}
			WebSocketWrapper.ordersNS.to(req.body?.IDCompany).emit("new-order", req.body);
			console.log(req.body);
			OrdersTimeoutService.addOpenedOrder(req.body?.IDOrder, req.body?.IDCompany);

			res.status(200).json({ message: "OK!" });
		} catch (error) {
			console.log(error);
			next(new HttpException(400, error));
		}
	}

	async updateStatus(req: any, res: Response, next: NextFunction): Promise<void> {
		try {
			if (!req.params.id) {
				throw Error("ID da empresa inexistente ou inválido");
			}
			const response = await OrdersService.updateStatus(req.params.id, req.company.ID, req.body);
			res.status(200).json(response);
		} catch (error) {
			console.log(error);
			next(new HttpException(400, error));
		}
	}
}
export default new OrdersController();
