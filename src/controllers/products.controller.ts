import { NextFunction, Request, Response } from "express";
import { HttpException } from "../exceptions/http.exception";
import { ProductsService } from "../services/products.service";
export class ProductsController {
  async getProducts(
    req: any,
    res: Response,
    next: NextFunction
  ): Promise<void> {
    try {
      const response = await ProductsService.getProducts(req.company.ID);
      res.status(200).json(response);
    } catch (error) {
      console.log(error);
      next(new HttpException(400, error));
    }
  }
  async getProductPopulated(
    req: any,
    res: Response,
    next: NextFunction
  ): Promise<void> {
    try {
      const response = await ProductsService.getProductPopulated(
        req.company.ID
      );
      res.status(200).json(response);
    } catch (error) {
      console.log(error);
      next(new HttpException(400, error));
    }
  }
  async getGroupOptions(
    req: any,
    res: Response,
    next: NextFunction
  ): Promise<void> {
    try {
      const response = await ProductsService.getGroupOptions(req.company.ID);
      res.status(200).json(response);
    } catch (error) {
      console.log(error);
      next(new HttpException(400, error));
    }
  }
  async getSizeInfo(
    req: any,
    res: Response,
    next: NextFunction
  ): Promise<void> {
    try {
      const response = await ProductsService.getSizeInfo(req.company.ID);
      res.status(200).json(response);
    } catch (error) {
      console.log(error);
      next(new HttpException(400, error));
    }
  }
  async changeStatus(
    req: any,
    res: Response,
    next: NextFunction
  ): Promise<void> {
    try {
      const response = await ProductsService.changeStatus(
        req.body,
        req.company.ID
      );
      res.status(200).json(response);
    } catch (error) {
      console.log(error);
      next(new HttpException(400, error));
    }
  }
  async updateVariety(
    req: any,
    res: Response,
    next: NextFunction
  ): Promise<void> {
    try {
      console.log(req.body);

      const response = await ProductsService.updateVariety(
        req.body,
        req.company.ID
      );
      res.status(200).json(response);
    } catch (error) {
      console.log(error);
      next(new HttpException(400, error));
    }
  }
}
export default new ProductsController();
