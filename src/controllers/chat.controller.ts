import { NextFunction, Request, Response } from "express";

interface Req extends Request { [key: string]: any }; 

export class ChatController {

    async listChats(req: Req, res: Response, next: NextFunction): Promise<void> {
        // try {
		// 	const response = await ChatsService.listProducts(req.company.ID);
		// 	res.status(200).json(response);
		// } catch (error) {
		// 	console.log(error);
		// 	next(new HttpException(400, error));
		// }
    }
    
    async getChat(req: Req, res: Response, next: NextFunction): Promise<void> {}
    
    async sendMessage(req: Req, res: Response, next: NextFunction): Promise<void> {}

}