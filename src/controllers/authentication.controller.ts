import { NextFunction, Request, Response } from "express";
import { HttpException } from "../exceptions/http.exception";
import { AuthenticationService } from "../services/authentication.service";

const Service = new AuthenticationService();

export class AuthenticationController {
	async login(req: Request, res: Response, next: NextFunction): Promise<void> {
		const { email, password } = req.body;
		try {
			const response = await AuthenticationService.login(email, password);

			res.status(200).json(response);

			// if (auth.error) {
			// 	console.log(auth.error);
			// 	next(new HttpException(auth.error.code, auth.error.message));
			// } else res.status(200).json(auth);
		} catch (error) {
			console.log(error);
			next(new HttpException(400, error));
		}
	}

}

export default new AuthenticationController();
